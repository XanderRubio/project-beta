# CarCar

Team:

* Bea Klein - Service | [LinkedIn](https://www.linkedin.com/in/bea-klein-82bb05254/) | [GitLab](https://gitlab.com/BeaKlein)
* Xander Clemens - Sales | [LinkedIn](https://www.linkedin.com/in/alexanderclemens/) | [GitLab](https://gitlab.com/XanderRubio)

## Overview

This app is the ultimate solution for car dealerships looking to streamline their data management. Through the Sales feature, you can effortlessly add salespeople and customers, enabling you to track which vehicles each sales rep has sold and which vehicles each customer has purchased. The Services feature allows you to add technicians and schedule appointments for vehicle maintenance, giving you complete control over appointment management. Additionally, the feature highlights VIP customers based on their vehicle identification number (VIN). Finally, with the Inventory feature, you can easily add manufacturers, make vehicle models, and view a comprehensive list of the dealership's inventory.

## Design
<img src="Design.png" width="1000" height="500" alt="alternative_text_for_image">

## Service microservice

My service microservice consists of three models and one Value Object: Appointment, Technician, Status, and AutomobileVO.
Technician and Status are Foreign Keys to the Appointment model. Adding Status as an additional Foreign Key allowed me to easily change status on an appointment without having to write extensive views. The integration points with the Inventory are both the customer field and the "sold" fields.

## Sales microservice

I first approached my microservice by building out the models. As I progressed with my design, I would reference and draw out my plan using Excalidraw. This helped me better understand how my microservice would connect to our inventory and service microservices.

To begin with my design, I must identify how my Sales Microservice is connected with the Services and Inventory. This is done through the Automobile Value Object. Since every Automobile in our database has a 17-digit VIN specifically unique to each car, my partner and I used the Automobile Value Object. After identifying the Automobile as a Value Object, I constructed my sales models. I used four models: AutomobileVO, SalesPerson, Customer, and RecordOfSale.

For the AutomobileVO, I used two fields of VIN specific to each car in our database and sold. This is because if the car is sold. Users will know whether the vehicle has already been listed as sold and prevent a double selling situation. For the SalesPerson, I used the fields first name, last name, and the Employee's ID. For the Customer, I used the fields first/last name, address, and phone number to collect our Customer's contact info. Lastly, for the Record of sales, I included the price field and, in addition, three foreign keys in the form of Automobile, salesperson, and Customer. The first foreign key, "automobile," was used to connect this model to my AutomobileVO model. This allows for easy access to the automobile data associated with the Record of a sale. The second foreign key, "sales_person," was used to connect the model to the SalesPerson model, allowing easy access to the data related to the salesperson associated with the Record of sale. The third foreign key, "customer," connects the model to the Customer model, allowing easy access to the data associated with the Record of sale.

After considering what would happen if a customer or salesperson were deleted from our database, I set the on_delete parameter to PROTECT to save the sales record. I did this because I imagine that in a real word business situation, having complete sales records is an important metric to measure any data analysis for the business.

From these models, I moved on to my admin and encoders to input my data into their respective databases and the Django Admin databases. From here, I created view functions to perform the following abilities of "POST," "GET," "PUT," and "DELETE" for my AutomobileVO, SalesPerson, Customer, and RecordOfSale. This allowed for my form functions to be able to Create a salesperson, Customer, and a record of a sale. In addition, the ability to list a sales history, the sales team members, and customers.

## Initialization
To start with the application, follow the following steps:

1. Simply fork and clone the repository to your local system. 

2. Then, open a terminal and run the command "docker-compose build" to build the necessary images. 

3. Once the build process is complete, run the command "docker-compose up" to start the containers and get the application up and running. 

Please take some time to get to know the project and its features by exploring it thoroughly.

